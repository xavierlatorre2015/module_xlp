class module_xlp::createfiles
{
  file { '/var/www/myproject/index.php':
    ensure  => 'present',
    source  => "puppet:///modules/module_xlp/index.php",
    mode    => '0644',
  }

  file { '/var/www/myproject/info.php':
    ensure  => 'present',
    source => "puppet:///modules/module_xlp/info.php",
    mode    => '0644',
  }
}
